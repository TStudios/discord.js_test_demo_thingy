/*
    Discord.JS Example for MorganDGamer
    Copyright (c) 2017-2020 0J3_0. All Rights Reserved.
    Copyright (c) 2020 MorganDGamer. All Rights Reserved.
*/

// Define statuses - not in config as i wana keep that smaller
const statuses = [
    {
        "type": "LISTENING",
        "name": "to Harlington Railways",
        "botstatus": "Idle"
    },
    {
        "type": "LISTENING",
        "name": "to Harlington Railways again",
        "botstatus": "Idle"
    },
]

// define setstatus
let status = 0
function setstatus() {
    status=status+1
    if (status == statuses.length) {status=0}
    client.user.setStatus(statuses[status].botstatus)
    client.user.setPresence({
        game: statuses[status]
    });
}


// Clear the terminal
console.clear()

// Print loading to the console
console.log("Loading...")

// Load Chalk
const chalk = require("chalk")

// Define console functions
function info(text) {
    console.log(chalk.blue("i") + " " + text)
}
function error_log(text) {
    console.log(chalk.red("e") + " " + text)
}

// Let user know how info logs look
info(chalk.rgb(255,255,255)("Info logs look like this"))

// Load config
info("Loading Config.JS...");
const config = require("./config.js");

// Load path
info("Loading Path...");
const path = require("path");
global.path=path

// Load Discord.JS
info("Loading Discord.JS...")
const Discord = require('discord.js');

// Load fs
info("Loading fs...")
const fs = require('fs');
global.fs=fs

// Create client - in a way, a bit like `Instance.new` in ROBLOX
/*
    new - operator
    Using `new` in Javascript loads the class after it, in this case a Discord Client
    A class's constructor runs upon running new [thingy]
    More information: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/new
*/
info("Creating Client...")
const client = new Discord.Client();

// Setting Per Server Config stuff
info("Setting Per Server Config stuff")

global.data = {}

global.LoadData = function(server_id) {
    if(!data[server_id]) {
        data[server_id]={}
    }
    fs.stat("./data/" + server_id + ".json", function(err,stat) { 
        if (err == null) { 
            data[server_id] = require("./data/" + server_id + ".json")
        } else if(err && err.code === 'ENOENT') {
            data[server_id] = {}
            fs.writeFile('./data/' + server_id + ".json","{}", function() {
        
            })
        } else {
            error_log("An error has occured while getting file:\n" + e)
            process.exit(5)
        }
    });
    return data[server_id]
}
global.EnsureData = function(server_id) {
    fs.stat("./data/" + server_id + ".json", function(err,stat) {
        if(err && err.code === 'ENOENT') {
            LoadData(server_id)
        } else {
            if (typeof(JSON.stringify(data[server_id])) == "undefined" || JSON.stringify(data[server_id]) == undefined) {
                try {
                    data[server_id] = require("./data/"+server_id+".json")                    
                } catch (error) {
                    
                }
            }
        }
    })
    if (typeof(JSON.stringify(data[server_id])) == "undefined" || JSON.stringify(data[server_id]) == undefined) {
        data[server_id]={}
    }
    /*if(typeof(data[server_id]) == 'undefined') {
        LoadData(server_id)
    }*/
}
global.SaveData = function(server_id) {
    EnsureData(server_id)
    if (typeof(data[server_id]) != "object") {
        error_log("Data is not an object!")
        process.exit(90)
    }
    fs.writeFile('./data/' + server_id + ".json",JSON.stringify(data[server_id]), function() {

    })
    fs.writeFile('./data/complete.json',JSON.stringify(data), function() {

    })
}



// Make info(),client, config, chalk and discord.js globally accessible
// eval is like loadstring in lua
// globals can be accessed as if they were variables from any required module, without the `global.[name]` part
function makeGlobal(a) {
    global[a]=eval(a)
}
makeGlobal("info")
makeGlobal("Discord")
makeGlobal("config")
makeGlobal("chalk")
makeGlobal("client")
makeGlobal("error_log")

// Load Command Handler
info("Loading Command Handler...")
const cmd_handler = require('./command_handler.js');

/*
    Set Event for ready - basically [bindableEvent Loaded].event:connect(function() end)
*/
info("Registering ready event")
client.once('ready', () => {
    info(chalk.green('Ready!'));
    info(chalk.rgb(50,50,255)('Bot Info:'));
    info(chalk.rgb(50,50,255)(`  ID: ${client.user.id}`))
    info(chalk.rgb(50,50,255)(`  Username & Tag: ${client.user.tag}`))
    info(chalk.rgb(50,50,255)(`  Prefix: ${config.defaultprefix}`))
    if (!client.user.bot) {console.log(chalk.red.bold("e") + " User is not a bot"); process.exit(60)} // ToS guarantee
    setstatus()
    setInterval(setstatus,7000)
});

/*
    Set Event for message - basically [bindableEvent Message].event:connect(function(msg) end)
*/
info("Registering message event")
client.on('message', message => {
    EnsureData(message.guild.id)
    SaveData(message.guild.id)
    info(chalk.blueBright("Server: " + message.guild.name + ", ") + chalk.greenBright("#" + message.channel.name + ", ") + chalk.redBright(message.author.tag + ": ") + message.content);
    let files = []
    message.attachments.each(element => {
        info(chalk.bgWhite.bold.rgb(0,0,0)("File found: " + element.attachment))
    });
    cmd_handler(message)
});

client.on("guildUpdate", guild => {
    SaveData(guild.id)
})

client.on("guildDelete", guild => {
    data[guild.id] = {}
    SaveData(guild.id)
})

if (!config.token) {
    throw new Error("No Token Provided")
}

/*
    Log into the client
*/
info("Logging into the bot account")
client.login(config.token);
