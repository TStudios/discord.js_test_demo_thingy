// Load File System | This is a built in Node.JS module so no need to install it manually!
info("Loading fs...")
const fs = require('fs');
client.commands = new Discord.Collection();

// Loop for all files ending in .js in commands
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

// 
for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.name, command);
}

module.exports = function(message) {
    if (message.content.toLowerCase().includes("ping")) {
        client.commands.get("ping").execute(message)
    }

    if (!message.content.startsWith(config.defaultprefix) || message.author.bot) return;

    const args = message.content.slice(config.defaultprefix.length).split(/ +/);
    const command = args.shift().toLowerCase();
    
    if (!client.commands.has(command)) return;

    try {
        client.commands.get(command).execute(message, args);
    } catch (error) {
        console.error(error);
        message.reply('there was an error trying to execute that command!');
    }
}